<?php


$client = new \GuzzleHttp\Client();
$response = $client->get('http://api.iebc.or.ke/geojson/county_17.geojson');
$content = json_decode($response->getBody()->getContents(), true);
$data = $content['features'][0]['geometry']['coordinates'];
$mapped = collect($data)->map(function ($data) {
    return collect($data)->map(function ($node) {
        return [
            "lat" => $node[1],
            "lng" => $node[0],
        ];
    });
})->first->flatten();


\Illuminate\Support\Facades\View::share('county', $mapped);
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/county/{url}', function ($url) {
    $county = \App\County::all()->find($url);
    $client = new \GuzzleHttp\Client();
    $response = $client->get($county->polygon);
    $content = json_decode($response->getBody()->getContents(), true);
    $data = $content['features'][0]['geometry']['coordinates'];
    $mapped = collect($data)->map(function ($data) {
        return collect($data)->map(function ($node) {
            return [
                "lat" => $node[1],
                "lng" => $node[0],
            ];
        });
    })->first->flatten();
    return response()->json(['c' => $mapped, 'center' => $county->center]);
});
Route::get('/constituencygeo/{url}', function ($url) {
    $county = \App\Constituency::all()->find($url);
    $client = new \GuzzleHttp\Client();
    $response = $client->get($county->polygon);
    $content = json_decode($response->getBody()->getContents(), true);
    $data = $content['features'][0]['geometry']['coordinates'];
    $mapped = collect($data)->map(function ($data) {
        return collect($data)->map(function ($node) {
            return [
                "lat" => $node[1],
                "lng" => $node[0],
            ];
        });
    })->first->flatten();
    return response()->json(['c' => $mapped, 'center' => $county->center]);
});
Route::get('/wardco/{url}', function ($url) {
    $county = \App\Ward::all()->find($url);
    $client = new \GuzzleHttp\Client();
    $response = $client->get($county->polygon);
    $content = json_decode($response->getBody()->getContents(), true);
    $data = $content['features'][0]['geometry']['coordinates'];
    $mapped = collect($data)->map(function ($data) {
        return collect($data)->map(function ($node) {
            return [
                "lat" => $node[1],
                "lng" => $node[0],
            ];
        });
    })->first->flatten();
    return response()->json(['c' => $mapped, 'center' => $county->center]);
});

Route::resource('constituency','ConstituencyController');
Route::resource('ward','WardController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
