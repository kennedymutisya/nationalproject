<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Constituency extends Model
{
    public $fillable = ['name'];

    public function ward()
    {
        return $this->hasMany(Ward::class);
}
}
