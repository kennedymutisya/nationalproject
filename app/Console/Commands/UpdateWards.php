<?php

namespace App\Console\Commands;

use App\Ward;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;

class UpdateWards extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ward:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Wards Data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Get all wards from iebc
        $wards = Cache::remember('wards', '43434434343434434', function () {
            $client = new Client();
            $response = $client->get("http://api.iebc.or.ke/ward/?&token=afd3877583a07e5b77e447332bb98a80");
            return json_decode($response->getBody()->getContents(), true);
        });
        $wards = collect($wards['region']['locations']);
        $wards->each(function ($ward) {
           Ward::where('name', $ward['name'])->update([
                'polygon'    => $ward['polygon'],
                'code'       => $ward['code'],
                'registered' => $ward['registered'],
                'center'     => json_encode($ward['center']),
            ]);

        });

    }
}
