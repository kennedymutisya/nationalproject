<?php

namespace App\Console\Commands;

use App\County;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class SaveCounties extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'project:county';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'get all counties';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $counties = \Cache::remember('county', '30000000000000000', function () {
            $client = new Client();
            $response = $client->get('http://api.iebc.or.ke/county/?token=afd3877583a07e5b77e447332bb98a80');
            return json_decode($response->getBody()->getContents(), true);

        });

        collect($counties['region']['locations'])->each(function ($county) {
            $saveCounty = new County();
            $saveCounty->polygon = $county['polygon'];
            $saveCounty->registered = $county['registered'];
            $saveCounty->code = $county['code'];
            $saveCounty->name = $county['name'];
            $saveCounty->center = json_encode($county['center']);
            $saveCounty->save();
        });
        $this->comment('Done getting all counties');
    }
}
