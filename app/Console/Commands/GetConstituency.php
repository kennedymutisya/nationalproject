<?php

namespace App\Console\Commands;

use App\County;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class GetConstituency extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'project:constituency';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get all constituencies for each county';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $county = County::all();
        $county->each(function ($county) {
            // Get all the constituencies
            $client = new Client();
            // Remove spaces and slashes
            $k = $county->name;
            $k = preg_replace('/\s/', '', $k);
            $k = preg_replace('/\//', '', $k);
            $k = preg_replace('/\'/', '', $k);
            $k = preg_replace('/\s*-\s*/', '-', $k);
            $countyname = mb_strtolower($k);
            $response = $client->get("https://raw.githubusercontent.com/mikelmaron/kenya-election-data/38f59ef765a0f0d6c2e82d84dec049b38ec6012f/data/httpvoteiebcorkeconstituencycounty{$countyname}");
            $constituencies = json_decode($response->getBody()->getContents(), true);

            collect($constituencies['results'])->each(function ($result) use ($county) {
                $county->constituency()->create([
                    'name' => $result['name']
                ]);
            });


        });
        $this->comment('Done saving all constituencies');
    }
}
