<?php

namespace App\Console\Commands;

use App\Constituency;
use App\Ward;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;

class UpdateConstituency extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'constituency:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Get all wards from iebc
        $wards = Cache::remember('constituencyupdate', '43434434343434434', function () {
            $client = new Client();
            $response = $client->get("http://api.iebc.or.ke/constituency/?&token=afd3877583a07e5b77e447332bb98a80");
            return json_decode($response->getBody()->getContents(), true);
        });
        $wards = collect($wards['region']['locations']);
        $wards->each(function ($ward) {
            Constituency::where('name', $ward['name'])->update([
                'polygon'    => $ward['polygon'],
                'code'       => $ward['code'],
                'registered' => $ward['registered'],
                'center'     => json_encode($ward['center']),
            ]);
            $this->comment("retrieved {$ward['name']}");
        });
    }
}
